<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'bitnami_wordpress');

/** MySQL database username */
define('DB_USER', 'bn_wordpress');

/** MySQL database password */
define('DB_PASSWORD', '248abbdcea');

/** MySQL hostname */
define('DB_HOST', 'localhost:3306');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'c311de2bd49a22a00bdc01d07f1fc33111441d0d02810349fb520f51f7095ff4');
define('SECURE_AUTH_KEY', '9a417ad48d9dfad19eaac40e19f34f5a4850a9f0e11c0f1f3ca56f748ba5f638');
define('LOGGED_IN_KEY', '17cd68f7032e996ccda28cc9a2b0d7ce22b9af62af579fb3739f8ef42613bd16');
define('NONCE_KEY', '34ce69ba74345476da3cb3bbf429554926016a4116c4ed8bc3ad4421629e61f4');
define('AUTH_SALT', 'd8fd480cb9ff98647d125622b90766cf878cacdc030ea97a428e819fc2007f24');
define('SECURE_AUTH_SALT', 'e8b6b263bc04f882ba2eddeb716454b5353198a8437a70036e6a7aff5b5b6f02');
define('LOGGED_IN_SALT', '392e6c0a5b77160fefc71212bdf04ab154d71e08f86b9d31461c7a6c8edf3b3c');
define('NONCE_SALT', '966ce97b2478418ce196303dcf4f517e9e8adad7b8c6c0472b69ebbf04ebb455');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */
/**
 * The WP_SITEURL and WP_HOME options are configured to access from any hostname or IP address.
 * If you want to access only from an specific domain, you can modify them. For example:
 *  define('WP_HOME','http://example.com');
 *  define('WP_SITEURL','http://example.com');
 *
*/

define('WP_SITEURL', 'http://ibm.polymath.io/');
define('WP_HOME', 'http://ibm.polymath.io/');


/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

define('WP_TEMP_DIR', '/opt/bitnami/apps/wordpress/tmp');


define('FS_METHOD', 'ftpext');
define('FTP_BASE', '/opt/bitnami/apps/wordpress/htdocs/');
define('FTP_USER', 'bitnamiftp');
define('FTP_PASS', '9OyOLLLFqYcIEGD8nfdtIvgpy4vocEycNxDZYmJivywbQXpPeX');
define('FTP_HOST', '127.0.0.1');
define('FTP_SSL', false);

