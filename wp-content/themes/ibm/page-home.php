<?php
/*
Template Name: Home
*/
get_header(); ?>
	<section class="hero" style="background-image: url('<?php echo get_template_directory_uri(); ?>/public/images/HOME-hero.jpg')">	
		<div class="batter vertical-align--table">
			<div class="vertical-align--table-cell ta-center">
				<h1>The Tides are Changing</h1>
				<a href="/tech-solution/e-bond/"><h2>Is your Brokerage <span class="blue-underline">E-bond</span> Ready?</h2></a>

				<div class="c2a">
					<a href="/bonds/getting-started/" class="btn-outline">
						Getting Started
					</a>
					<a href="http://www.intlbondmarine.com/registration.aspx" class="btn-outline">
						Already a Customer
					</a>
				</div>
			</div>
		</div>
	</section>
	<section class="gut">
		<div class="container">
			<div class="grid">
				<div class="col-66 col--centered">
					<h3>International Bond and Marine is Ready</h3>
					<p>We are experiencing a seismic shift in how Customs conducts their business. IB&M is ready with our state-of-the-art technology, robust reporting for analytics, industry-leading customer service, and creative underwriting solutions. Read more and learn why we are the preferred industry choice and leader.</p>
				</div>
				<div class="col-33 bonds">
					<a href="/bonds"><i>
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="484.8 285.9 229.4 229.3" enable-background="new 484.8 285.9 229.4 229.3"><path fill="#30ACE3" d="M587.2 328.2l-6.7-42.1c-49.4 8.2-88.3 47.5-95.7 97.1l42.1 6.7c4.5-31.3 29.1-56.5 60.3-61.7z"/><path fill="#244164" d="M714.2 383.5c-7.4-50.1-46.7-89.8-96.6-97.6l-6.7 42.1c31.8 5 57 30.2 61.4 62.2l41.9-6.7"/><path fill="#244164" d="M586.9 472.8c-30.1-5.1-53.9-28.8-59.5-58.7l-42.1 6.7c8.5 48.2 46.6 85.9 94.9 94.1l6.7-42.1z"/><path fill="#30ACE3" d="M713.8 420.6l-42.1-6.7c-5.6 30.4-29.9 54.2-60.5 59.2l6.7 42.1c48.7-7.8 87.4-46.1 95.9-94.6"/></svg>
					</i></a>
					<h3>Bonds</h3>
					<p>Serving Customs brokers, freight forwarders, and carnet holders, we offer competitively priced bonds with incomparable service and support around-the-clock to meet our clients’ needs.</p>
					<a href="/bonds" class="btn-blk btn-outline">Learn More</a>
				</div>
				<div class="col-33 insurance">
					<a href="/insurance"><i>
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="486 279.2 231 231" enable-background="new 486 279.2 231 231"><path fill="#244164" d="M649.3 324c-10.7-2.1-26.4-3.8-49.4-3.8-55.4 0-69.1 10.4-69.1 10.4v72.6c0 9.9 4 20 10.1 29.4 0 0 5.8.6 55.8-49 50.2-49.8 52.6-59.6 52.6-59.6z"/><path fill="#30ACE3" d="M558.4 448.5c19.2 19 41.3 32.3 41.3 32.3s69.1-41.4 69.1-83v-59.7s-7 13-53.6 60.6c-46.6 47.4-56.8 49.8-56.8 49.8zM693.8 301.8c1.6 1.1 2.9 3.5 2.9 5.4v93.4c0 55.4-93.9 109-93.9 109-1.6 1-4.3 1-6.1 0 0 0-93.9-53.6-93.9-109v-93.4c0-1.9 1.3-4.3 2.9-5.4 0 0 16.5-11.8 94.1-11.8 77.3-.1 94 11.8 94 11.8zm-94.1 2c-66.4 0-83 12.2-83 12.2v84.8c0 48.5 83 96.8 83 96.8s83-48.5 83-96.8v-84.8s-16.6-12.2-83-12.2z"/></svg>

					</i></a>
					<h3>Insurance</h3>
					<p>You always wonder if you need it until something goes wrong and realize how critical it is to have it with a partner who understands international trade and works to alleviate the pain of a potential loss/claim.</p>
					<a href="/insurance" class="btn-blk btn-outline">Learn More</a>
				</div>
				<a href="/technology"><div class="col-33 technology">
					<i>
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="487.8 288.9 224.7 225.6" enable-background="new 487.8 288.9 224.7 225.6"><path fill="#244164" d="M627 436c0 14.9-12.2 27-27 27s-27-12.2-27-27 12.2-27 27-27c14.9 0 27 12.1 27 27z"/><path fill="#30ACE3" d="M650.2 436c0 27.8-22.6 50.4-50.4 50.4-27.8 0-50.4-22.6-50.4-50.4s22.6-50.4 50.4-50.4c28 0 50.4 22.6 50.4 50.4zm-50.2-40c-22.1 0-40 17.9-40 40s17.9 40 40 40 40-17.9 40-40-17.9-40-40-40z"/><path fill="#244164" d="M674.2 436.2c0 41.1-33.4 74.6-74.6 74.6s-74.6-33.3-74.6-74.6c0-41.1 33.3-74.6 74.6-74.6 41.2.2 74.6 33.4 74.6 74.6zm-74.5-59.2c-32.6 0-59.2 26.6-59.2 59.2s26.6 59.2 59.2 59.2 59.2-26.6 59.2-59.2-26.4-59.2-59.2-59.2zM522.7 358.1c0 6.9-5.6 12.3-12.3 12.3-6.9 0-12.3-5.6-12.3-12.3 0-6.9 5.6-12.3 12.3-12.3 6.7-.2 12.3 5.4 12.3 12.3z"/><path fill="#30ACE3" d="M533.4 358.1c0 12.8-10.4 23-23 23-12.8 0-23-10.4-23-23s10.4-23 23-23c12.6-.2 23 10.2 23 23zm-23.2-18.4c-10.1 0-18.4 8.2-18.4 18.4s8.2 18.4 18.4 18.4c10.1 0 18.4-8.2 18.4-18.4s-8.1-18.4-18.4-18.4z"/><path fill="#244164" d="M695 327.2c0 11.5-9.3 20.8-20.8 20.8s-20.8-9.3-20.8-20.8 9.3-20.8 20.8-20.8 20.8 9.4 20.8 20.8z"/><path fill="#30ACE3" d="M713 327.2c0 21.3-17.3 38.7-38.7 38.7-21.3 0-38.7-17.3-38.7-38.7 0-21.3 17.3-38.7 38.7-38.7 21.2.1 38.7 17.4 38.7 38.7zm-38.8-30.7c-17 0-30.7 13.8-30.7 30.7s13.8 30.7 30.7 30.7 30.7-13.8 30.7-30.7c.1-17-13.7-30.7-30.7-30.7z"/><path fill="#244164" d="M618.6 419.7c-3.4 4.8-8.5 7-11.5 5-3-2.1-2.9-7.8.5-12.6l49.6-71.4c3.4-4.8 8.5-7 11.7-5 3 2.1 2.7 7.7-.5 12.6l-49.8 71.4zM539 374.6c1.1 1 0 4-2.6 6.9-2.6 2.7-5.4 4.2-6.6 3l-16.3-15.2c-1.1-1 0-4.2 2.6-6.9s5.4-4.2 6.6-3l16.3 15.2z"/></svg>
					</i></a>
					<h3>Solutions</h3>
					<p>Our technology meets the dual responsibility of protecting our customers and complying with US Customs’ regulations. Our systems are designed to offer visibility into their client performance and limit keystrokes to increase productivity. We utilize the most advanced data security systems available to ensure confidentiality for our trade partners.</p>
					<a href="/technology" class="btn-blk btn-outline">Learn More</a>
				</div>
			</div>
		</div>
	</section>
<?php get_footer(); ?>