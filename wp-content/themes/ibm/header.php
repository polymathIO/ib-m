<!doctype html>
<html lang="en">
  <head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php wp_title( ' | ', true, 'right' ); ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/slick/slick.css"/>
	<?php wp_head(); ?>
    <!--[if lt IE 9]>
      <script type="text/javascript" src="http://html5shiv.googlecode.com/svn/trunk/html5.js">
      </script>
    <![endif]-->      
  </head>
  <?php 
  	if ( is_page( 'insurance' ) || is_page( 'bonds' ) || is_page( 'technology' ) ) {
  		$class = "internal";
  	} 
  	if ( is_page( 'about' )) {
  		$class = "about";
  	} ?>
  <body class="<?php echo $class; ?>">
    
    <div class="lightbox vertical-align--table">
	<div class="vertical-align--table-cell ta-center">
		<div class="mainbox">
			<a class="close">x</a>
			<div class="loginbox">
			<h2>Login</h2>
				<form>
					<input type="text" placeholder="Username" />
					<input type="password" placeholder="**********" />
					<input type="submit" />
				</form>
			</div>
		</div>
	</div>
</div>


<nav>
  <div class="container">
  	<!--<?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?>-->
  	<a href="/"class="logo">International Bond & Marine</a>
  	<a href="/bonds">Bonds</a>
  	<a href="/insurance">Insurance</a>
  	<a href="/technology">Solutions</a>
  	<a href="/about">About</a>
  	<!--a href="/contact">Contact</a-->
  	<a class="login" href="https://www.intlbondmarine.com/U_Login.aspx">Login 
      <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 20 20" enable-background="new 0 0 20 20" xml:space="preserve">
        <g>
          <path fill="#8E304E" d="M0.8,5.8c4.4,3.6,13.6,10.9,15.7,12.4c0,0,0,0-0.1,0.1c-1.1,1-2.8,1.7-5,1.7H0.8v-6.7
            c3.6,2.7,6.9,5,7.4,4.8c-0.4-0.1-3.7-2.8-7.4-5.7v-2.4c4.8,3.6,12.4,8.9,12.9,8.6c-0.2-0.1-8.3-6.2-12.9-9.9V5.8z"></path>
          <path fill="#251B1E" d="M18.3,14c0,1.5-0.6,3-1.7,4.1C14.7,16.3,5.1,7.9,0.8,4.3V0.9c4.5,3.7,14.3,12.8,15,13.8
            C15.9,14,6.2,4.3,1.5,0.2h2.6c4.3,3.6,9.1,7.7,9.4,8c0-0.5-4.1-4.4-8.2-8h5.4c3,0,4.9,1.4,5.7,3.3c0.6,1.3,0.7,2.7,0.1,3.9
            c-0.4,0.7-0.9,1.3-1.8,1.8c0.7,0.3,1.3,0.6,1.8,1C17.8,11.1,18.3,12.6,18.3,14z"></path>
        </g>
      </svg>
    </a>
  </div>
</nav>
