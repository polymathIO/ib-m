<?php
/*
Template Name: About
*/
get_header(); ?>
    
	<section class="hero" style="background-image: url(<?php the_field("hero_image") ?>)">	
		<div class="batter vertical-align--table">
		<?php the_breadcrumb(); ?>
			<div class="vertical-align--table-cell ta-center">
				<h1 class="noline">International Bond & Marine</h1>
			</div>
		</div>
	</section>
	<section class="people">
		<div class="container">
			<div class="grid">
				<?php if( have_rows('team') ): while ( have_rows('team') ) : the_row(); ?>
					<div class="col-20">
						<div>
							<img src="<?php the_sub_field('photo'); ?>" />
							<aside>
								<a href="mailto:<?php the_sub_field("email"); ?>" class="email">
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="590.4 391.3 20 18.9" enable-background="new 590.4 391.3 20 18.9"><path fill="#61CAE6" d="M590.4 393.5v14.5h20v-14.5h-20zm10 9.3l2.5-1.8 5.3 5.6h-15.7l5.3-5.6 2.6 1.8zm0-2.6l-6.4-4.5h12.7l-6.3 4.5zm-3.2.3l-5.3 5.5v-9.2l5.3 3.7zm11.7 5.5l-5.3-5.5 5.3-3.7v9.2z"/></svg>
								</a>
							</aside>
						</div>
						<h4><?php the_sub_field('name'); ?></h4>
						<h5><a class="emaillink" href="mailto:<?php the_sub_field("email"); ?>"><?php the_sub_field('email'); ?></a></h5>
					</div>
				<?php endwhile; else : endif; ?>
			</div>
		</div>
	</section>
	<section class="gut">
		<div class="container">
			<div class="grid">
				<div class="col-66 clol--centered">
					<h2>About Us</h2>
					<?php the_field("about_us"); ?>
				</div>
				<div class="col-66 clol--centered">
					<h2>Our Mission</h2>
					<?php the_field("our_mission"); ?>
				</div>
				
			</div>
		</div>
	</section>
	
<?php get_footer(); ?>