        <footer>
    	<div class="container">
    		<div class="footer-top">
    			<a class="logo"></a>
<!--    			<a class="email-page">Email This Page</a>-->
    		</div>
    		<div class="grid">
    			<div class="col-25 stack">
    			
    			    			<h3>Recent News</h3>
    			
    			<ul>
    			<?php query_posts('showposts=3');?>
    			  <?php while (have_posts()) : the_post(); ?>
    			    <li><?php the_date(); ?></li>
    			    <li><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></li>
    			  <?php endwhile; ?>
    			
    			</ul>
    			<a href="/news-entries/" class="btn-outline">Older Entries</a><!--
    			<!--<div class="left">
    						<h2>Previous entries</h2>
    						<ul class="block">
    			<?php
    			$myposts = get_posts('numberposts=4&offset=1');
    			if ($myposts) :
    				foreach ($myposts as $post) :
    					setup_postdata($post);
    							unset($categories);
    					foreach (get_the_category() as $category)
    						$categories[] = $category->cat_name;
    			?>
    							<li><a href="<?php the_permalink();?>"><span><?php the_title(); ?></span><em><?php the_date();?> in <?=implode(", ",$categories);?></em></a></li>
    			<?php endforeach;?>
    			<?php else : ?>
    							<li>no entries</li>
    			<?php endif;?>
    						</ul>
    			</div>
    			--*>
    			
    			<div class=""><br />
    					<h3>About Us</h3>
    					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
    					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
    					quis nostrud exercitation ullamco.
    					</p>
    					<a class="readmore">Read More &raquo;</a>
    				</div>
    			
    				    -->
    			</div>

    			
    			<div class="col-75">
    				<div class="col-66 stack">
    				<div class="col-33">
    				<br />
    					<ul>
    						<li><a href="/">Home</a></li>
    						<li><a href="/about">About Us</a></li>
    						<li><a href="/get-a-quote">Get a Quote</a></li>
       					</ul>
    				</div>
    					<div class="col-66">
    					<ul>
    						<li><br />
    					<h3>Locations</h3>
    							<p><strong>New York</strong><br />
    							Two Hudson Place, 4th Floor<br />
    							Hoboken, NJ 07030<br />
    							Phone (201) 653-6100<br />
    							Fax (201) 653-7766</p>
    						</li>
    						<li>
    					    	<p><strong>San Francisco</strong><br />
    							1499 Bayshore Hwy., Ste 215<br />
    							Burlingame, CA 94010<br />
    							Phone (650) 692-3578<br />
    							Fax (650) 692-0567</p>
    						</li>
    					</ul>
    					</div>
    					
    				</div>
    
    				<div class="col-33 stack">
    					<h3>Contact</h3>
    					<div class="icons">
    						<a class="email" href="mailto:bond@intlbondmarine.com">bond@intlbondmarine.com</a>
    						<a class="phone" href="callto:1-201-653-6100">201-653-6100</a>
    						<a class="social" href="https://www.linkedin.com/company/international-bond-&-marine-brokerage-ltd?trk=company_logo">LinkedIn</a>
    					</div>
    				</div>

    				
    				
    			</div>
    		</div>  <!--end grid-->
    	</div>
    </footer>

    <script type="text/javascript" src="//use.typekit.net/gcy3ilt.js"></script>
    <script type="text/javascript">try{Typekit.load();}catch(e){}</script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/slick/slick.js"></script>    
	<script>
    	$(document).ready(function() {
	        $('.single-item').slick({
	            dots: true,
	            infinite: true,
	            speed: 300,
	            slidesToShow: 1,
	            slidesToScroll: 1
	        });
	    });
	</script>
    <script src="public/scripts/scripts.min.js"></script>
    <?php wp_footer(); ?>
  </body>
</html>