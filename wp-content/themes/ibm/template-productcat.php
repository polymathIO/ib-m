<?php
/*
Template Name: Product Types
*/
get_header(); ?>
	<section class="hero" style="background-image: url('<?php the_field("hero_image") ?>')">	
		<div class="batter vertical-align--table">
				<?php the_breadcrumb(); ?>
		
			<div class="vertical-align--table-cell ta-center">
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<?php 
					$svg = get_field('svg_icon');
					echo file_get_contents($svg);
				?>		
				<h1>IB&M <?php the_title(); ?></h1>
				<?php the_content(); ?>
				<div class="c2a">
					<a class="btn-outline" href="/get-a-quote">
						Get a Quote
					</a>
			<?php endwhile; else : ?>
				<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
			<?php endif; ?>
				</div>
			</div>
		</div>
	</section>
	<section class="gut">
		<div class="container">
			<div class="grid">
				<?php
				$type = 'bond';
				if (is_page( 'bonds' ) ){
					$type = 'bond';
				}
				if (is_page( 'technology' ) ){
					$type = 'tech-solution';
				}
				if (is_page( 'insurance' ) ){
					$type = 'insurance-product';
				}
					$args=array(
						'post_type' => $type,
						'order' => 'ASC'
					);
					$my_query = null;
					$my_query = new WP_Query($args);
					if( $my_query->have_posts() ) {
						while ($my_query->have_posts()) : $my_query->the_post(); ?>
						    <a href="<?php the_permalink(); ?>" class="productlink">
						    <div class="col-33">
								<i>
									<!--<svg xmlns="http://www.w3.org/2000/svg" viewBox="584.7 386.2 29.2 28.5" enable-background="new 584.7 386.2 29.2 28.5"><path fill="#74241D" d="M584.9 411.4c-.4-.5-.4-1.2.1-1.6l7.6-6.9c.5-.4 1.2-.4 1.6.1l2.5 2.8c.4.5.4 1.2-.1 1.6l-7.6 6.9c-.5.4-1.2.4-1.6-.1l-2.5-2.8zm18.7-25.2c1.1 0 2.2.2 3.3.6 5.4 1.8 8.2 7.7 6.4 13.1-1.5 4.2-5.5 7-9.7 7-1.1 0-2.2-.2-3.3-.6-5.4-1.8-8.2-7.7-6.4-13.1 1.5-4.3 5.5-7 9.7-7m0 2.8c-3.2 0-6.1 2-7.1 5.1-.6 1.9-.5 3.9.3 5.8.9 1.8 2.4 3.1 4.2 3.8.8.3 1.6.4 2.4.4 3.2 0 6.1-2 7.1-5.1.6-1.9.5-3.9-.3-5.8-.9-1.8-2.4-3.1-4.2-3.8-.7-.3-1.6-.4-2.4-.4z"/></svg>-->
									<?php 
										$svg = get_field('svg-icon');
										echo file_get_contents($svg);
									?>			
								</i>
								<h3><?php the_title(); ?></h3>
								<?php the_excerpt(); ?>
								<a href="<?php the_permalink(); ?>" class="btn-outline productlink">Learn More</a>
							</div>
							</a>
						  <?php
						endwhile;
					}
						wp_reset_query();  // Restore global post data stomped by the_post().
				?>
			</div>
		</div>
	</section>
<?php get_footer(); ?>