<?php
/*
Template Name: Product Types!
*/
get_header(); ?>
	<section class="hero" style="background-image: url('<?php echo get_template_directory_uri(); ?>/public/images/bonds.jpg')">	
		<div class="batter vertical-align--table">
			<div class="vertical-align--table-cell ta-center">
				<svg xmlns="http://www.w3.org/2000/svg" viewBox="484.8 285.9 229.4 229.3" enable-background="new 484.8 285.9 229.4 229.3"><path fill="#8BDCE3" d="M587.2 328.2l-6.7-42.1c-49.4 8.2-88.3 47.5-95.7 97.1l42.1 6.7c4.5-31.3 29.1-56.5 60.3-61.7z"/><path fill="#00E7FF" d="M714.2 383.5c-7.4-50.1-46.7-89.8-96.6-97.6l-6.7 42.1c31.8 5 57 30.2 61.4 62.2l41.9-6.7"/><path fill="#00E7FF" d="M586.9 472.8c-30.1-5.1-53.9-28.8-59.5-58.7l-42.1 6.7c8.5 48.2 46.6 85.9 94.9 94.1l6.7-42.1z"/><path fill="#8BDCE3" d="M713.8 420.6l-42.1-6.7c-5.6 30.4-29.9 54.2-60.5 59.2l6.7 42.1c48.7-7.8 87.4-46.1 95.9-94.6"/></svg>
				<h1>IB&M Bonds</h1>
				<p> Plain and simple, a bond is an insurance policy that offers protection.  Our bonds protect three distinct groups.  We protect shippers in the event of loss, damage or non-delivery of their cargo.  We protect Customs brokers, freight forwarders and OTI’s from legal action against them.  Finally, we protect government agencies for non-payment of duties or penalties.</p>
				<p>Regardless of whose interest we represent, our focus is to research the best and most appropriate coverage, offer the best underwriting solution, make sure it is competitively priced with the marketplace and provide attentive, professional proactive customer service if there is a claim against that bond.</p>

				<div class="c2a">
					<a class="btn-outline">
						Sign Up
					</a>
					<a class="btn-outline">
						Sign In
					</a>
				</div>
			</div>
		</div>
	</section>
	<section class="gut">
		<div class="container">
			<div class="grid">
				<div class="col-33">
					<i>
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="584.7 386.2 29.2 28.5" enable-background="new 584.7 386.2 29.2 28.5"><path fill="#74241D" d="M584.9 411.4c-.4-.5-.4-1.2.1-1.6l7.6-6.9c.5-.4 1.2-.4 1.6.1l2.5 2.8c.4.5.4 1.2-.1 1.6l-7.6 6.9c-.5.4-1.2.4-1.6-.1l-2.5-2.8zm18.7-25.2c1.1 0 2.2.2 3.3.6 5.4 1.8 8.2 7.7 6.4 13.1-1.5 4.2-5.5 7-9.7 7-1.1 0-2.2-.2-3.3-.6-5.4-1.8-8.2-7.7-6.4-13.1 1.5-4.3 5.5-7 9.7-7m0 2.8c-3.2 0-6.1 2-7.1 5.1-.6 1.9-.5 3.9.3 5.8.9 1.8 2.4 3.1 4.2 3.8.8.3 1.6.4 2.4.4 3.2 0 6.1-2 7.1-5.1.6-1.9.5-3.9-.3-5.8-.9-1.8-2.4-3.1-4.2-3.8-.7-.3-1.6-.4-2.4-.4z"/></svg>
					</i>
					<h3>Customs Bonds</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.</p>
					<a class="btn-outline">Learn More</a>
				</div>
				<div class="col-33">
					<i>
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="578 379.3 45 41.5" enable-background="new 578 379.3 45 41.5"><path fill="#90D5DE" d="M606.6 379.3h-24.2v41.5h35.2v-30.8l-11-10.7zm-.5 4.5l6.5 6.4h-6.5v-6.4zm7.8 33.5h-28v-34.3h17.4v10h10.6v24.3z"/></svg>
					</i>
					<h3>Carnet Bonds</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.</p>
					<a class="btn-outline">Learn More</a>
				</div>
				<div class="col-33">
					<i>
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="580 381 36 34.9" enable-background="new 580 381 36 34.9"><path fill="#1D6D74" d="M599.7 411.1c.5 0 .9.1 1.3.3.4.2.8.4 1.1.7.5.3 1 .7 1.4.7.4 0 .9-.3 1.4-.7l1.1-.7c.3-.1.7-.2 1-.2l4.2-13.7c.2-.7-.2-1.4-1-1.6l-.9-.1v-2.8c0-.7-.6-1.3-1.3-1.3h-1.4v-2c0-.7-.6-1.3-1.3-1.3h-3.2v-.7c0-.7-.6-1.3-1.3-1.3h-2.2c-.7 0-1.3.6-1.3 1.3v.7h-3.3c-.7 0-1.3.6-1.3 1.3v2h-1.4c-.7 0-1.3.6-1.3 1.3v2.8l-.9.1c-.7.1-1.2.8-1 1.6l4.2 13.7c.4 0 .7.1 1 .2.4.2.8.4 1.1.7.5.3 1 .7 1.4.7s.9-.3 1.4-.7l1.1-.7c.5-.2 1-.3 1.4-.3zm5.5-21.3c.3 0 .5.2.5.5v1.8c0 .3.2.5.5.5h1.9v2.7l-8.4-1.1-8.4 1.1v-2.7h1.9c.3 0 .5-.2.5-.5v-1.8c0-.3.2-.5.5-.5h11zm-5.5 24.4c.3 0 .7 0 .9.2 1 .5 1.9 1.4 2.9 1.4s1.9-.9 2.9-1.4c.8-.4 1.6-.1 2.5.3.2.1.4 0 .4-.2v-1.3c0-.3-.2-.6-.4-.8-.8-.4-1.7-.7-2.5-.3-1 .5-1.9 1.4-2.9 1.4s-1.9-.9-2.9-1.4c-.3-.1-.6-.2-.9-.2-.3 0-.7 0-.9.2-1 .5-1.9 1.4-2.9 1.4s-1.9-.9-2.9-1.4c-.8-.4-1.6-.1-2.5.3-.3.2-.4.4-.4.8v1.3c0 .2.3.3.4.2.8-.4 1.7-.7 2.5-.3 1 .5 1.9 1.4 2.9 1.4s1.9-.9 2.9-1.4c.3-.2.6-.2.9-.2z"/></svg>
					</i>
					<h3>Ocean Transportation Intermediary Bonds</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua.</p>
					<a class="btn-outline">Learn More</a>
				</div>
				<div class="col-33">
					<i>
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="582 384.4 35 31.5" enable-background="new 582 384.4 35 31.5"><g fill="#8BDCE3"><path d="M600.2 384.4c-4.5 0-8.3 3.7-8.3 8.3 0 1 .2 1.9.5 2.8v.1l7.8 20.3 7.6-19.9c.4-1 .7-2.1.7-3.3 0-4.5-3.8-8.3-8.3-8.3zm0 14.6c-3.4 0-6.3-2.8-6.3-6.3 0-3.4 2.8-6.3 6.3-6.3s6.3 2.8 6.3 6.3-2.9 6.3-6.3 6.3z"/><circle cx="600.2" cy="392.7" r="4.6"/></g></svg>
					</i>
					<h3>BMC 84 - Domestic Property Brokers Bonds</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua.</p>
					<a class="btn-outline">Learn More</a>
				</div>
				<div class="col-33">
					<i>
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="582 383.4 36 34.6" enable-background="new 585.1 383.4 33 34.6"><path fill="#74241D" d="M588.5 418.1l2.1-12.5-9.1-8.9 12.6-1.8 5.7-11.4 5.7 11.4 12.6 1.8-9.1 8.9 2.1 12.5-11.2-6-11.4 6zm11.3-7.9l9 4.7-1.7-10 7.3-7.1-10.1-1.5-4.5-9.1-4.5 9.1-10.1 1.5 7.3 7.1-1.7 10 9-4.7z"/></svg>
					</i>
					<h3>Military Performance Bonds</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.</p>
					<a class="btn-outline">Learn More</a>
				</div>
			</div>
		</div>
	</section>
<?php get_footer(); ?>