<?php
/*
Template Name: Getting Started
*/
get_header(); ?>

	<section class="hero" style="background-image: url('<?php echo get_template_directory_uri(); ?>/public/images/gettingstarted.jpg')">	
		<div class="batter vertical-align--table">
			<div class="vertical-align--table-cell ta-center">
				<h1>Getting Started With IB&M Is As Easy As 1-2-3</h1>
				<p>Insurance is a very personal thing for individuals based on their needs, and business insurance is no different.  The needs of a shipper moving metal castings from Asia by ocean are different than those of a jeweler moving gems from Europe by air, or perishable agriculture from South America.</p>

				<div class="c2a">
					<a class="btn-outline">
						Get A Quote
					</a>
					<a class="btn-outline">
						Create An Account
					</a>
				</div>
			</div>
		</div>
	</section>
	<section class="gut gettingstarted">
		<div class="container">
			<div class="grid">
				<div class="col-33">
					<i>
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="557.9 358.7 84.1 84.2" enable-background="new 557.9 358.7 84.1 84.2"><path fill="#8BDCE3" d="M589.9 371.9v-2.7h7.7v2.7c0 1.4 1.2 2.6 2.6 2.6h.7c1.4 0 2.6-1.2 2.6-2.6v-2.7h7.7v2.7c0 1.4 1.2 2.6 2.6 2.6h.7c1.4 0 2.6-1.2 2.6-2.6v-2.7h1.6l5.1 4.9v7.9h5.3v-10.1l-8.1-8h-3.7v-2.6c0-1.4-1.2-2.6-2.6-2.6h-.7c-1.4 0-2.6 1.2-2.6 2.6v2.6h-7.7v-2.6c0-1.4-1.2-2.6-2.6-2.6h-.7c-1.4 0-2.6 1.2-2.6 2.6v2.6h-7.8v-2.6c0-1.4-1.2-2.6-2.6-2.6h-.7c-1.4 0-2.6 1.2-2.6 2.6v2.6h-7.7v-2.6c0-1.4-1.2-2.6-2.6-2.6h-.7c-1.4 0-2.6 1.2-2.6 2.6v2.6h-12.4v79h23.2l1.3-5.3h-19.3v-68.5h7.2v2.7c0 1.4 1.2 2.6 2.6 2.6h.7c1.4 0 2.6-1.2 2.6-2.6v-2.7h7.7v2.7c0 1.4 1.2 2.6 2.6 2.6h.7c1.3.1 2.5-1.1 2.5-2.5z"/><path fill="#1D6D74" d="M640.4 398.7l-8.4-8.4c-2-2-5.4-2.1-7.4-.1l-30.7 30.7-.6.6-5.4 21.2 21.2-5.4 2-2 29.4-29.4c2-1.9 2-5.2-.1-7.2zm-44.9 24.9l11.6 11.6-10.9 2.7-3.4-3.5 2.7-10.8zm13.7 4.5l-6.6-6.6 22-22 6.6 6.6-22 22z"/></svg>
					</i>
					<h3>1. New Customer Application</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.</p>
					<a class="btn-blk btn-outline">Apply Online</a>
				</div>
				<div class="col-33">
					<i>
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="557.1 359 87.7 82.7" enable-background="new 557.1 359 87.7 82.7"><path fill="#1D6D74" d="M644.1 421v-.4l-13.2-42.7-.1-.3-.1-.2c0-.7-.6-1.3-1.3-1.3-.4 0-.8.2-1 .5-7.7-1.5-15.4-7.3-23-9.7v-.7c0-.8-.6-1.4-1.4-1.4h-.8l-.9-4.8c-.1-.6-.7-.9-1.3-.9-.6 0-1.2.4-1.3.9l-.9 4.8h-.8c-.8 0-1.4.6-1.4 1.4v.6c-7.9 2.3-15.7 8.3-23.5 9.8-.2-.3-.6-.5-1-.5-.7 0-1.2.5-1.3 1.2-.1.1-.2.2-.2.4l-13.3 43.1v.7c.2 6.5 6.8 11.6 14.9 11.6 8.2 0 14.7-5.2 14.9-11.6v-.7l-12.7-41.1c7.5-1 14.9-5.7 22.4-7v.2c0 .8.6 1.4 1.4 1.4h.1v56.9c-.5.2-.8.7-.8 1.3v2.1h-.5c-.8 0-1.4.6-1.4 1.3v.6c-4.6.2-9.2.7-13.7 1.5-.9.2-1.6.9-1.6 1.7v.5c0 .8.7 1.4 1.6 1.4h38.7c.9 0 1.6-.7 1.6-1.4v-.5c0-.8-.7-1.6-1.6-1.7-4.5-.8-9.1-1.3-13.7-1.5v-.5c0-.7-.6-1.3-1.4-1.3h-.4v-2.3c0-.6-.4-1.1-.9-1.3v-56.8h.2c.8 0 1.4-.6 1.4-1.4v-.1c7.3 1.4 14.5 5.9 21.9 6.9l-12.7 41.1v.7c.2 6.5 6.8 11.6 14.9 11.6s14.7-5.2 14.9-11.6v-.1c-.7-.3-.7-.4-.7-.4zm-60.5 0h-23.9l12-38.7 11.9 38.7zm33.6 0l12-38.7 12 38.7h-24z"/><path fill="#8BDCE3" d="M622.4 437.4c-5.1-.9-10.3-1.4-15.4-1.7v-.6c0-.8-.9-.7-1.7-.7h-.3v-3c0-.7 0-1.2-1-1.5v-.5h-7v4.9h-.9c-.9 0-2.1-.1-2.1.7v.6c-5.1.3-10.3.8-15.4 1.7-1 .2-1.8 1-1.8 1.9v.5c0 .9 1.5 1.5 2.4 1.5h42.8c1 0 2.1-.6 2.1-1.5v-.5c.1-.7-.8-1.6-1.7-1.8z"/></svg>
					</i>
					<h3>2. Limited POA</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.</p>
					<a class="btn-blk btn-outline">Upload POA</a>
				</div>
				<div class="col-33">
					<i>
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="553.4 356.4 93.8 88.2" enable-background="new 553.4 356.4 93.8 88.2"><path fill="#1D6D74" d="M592.8 394.9l-7.2 7.2 13.4 13.5 22.8-22.8-5.3-5.3-17.2 14.5-6.5-7.1z"/><path fill="#8BDCE3" d="M602.3 358.5c-23.3 0-42.3 20.3-42.7 43.5h-2.9l4.4 8.4 4.4-8.4h-3.6c.3-21.2 18.3-41.2 40.3-41.2 22.2 0 40.3 17.4 40.3 39.6s-18 40-40.2 40c-13.7 0-25.9-7.1-33.1-17.6l-1.9 1.3c7.7 11 20.5 18.4 35.1 18.4 23.5 0 42.7-18.5 42.7-42-.2-23.5-19.2-42-42.8-42zm0 86c-14.5 0-28.4-7.2-36.8-19.3l-1.2-1.7 5.4-3.7 1.2 1.7c7.2 10.4 19 16.7 31.4 16.7 21.1 0 38.3-17.1 38.3-38 0-10.2-3.9-19.7-11.2-26.8-7.2-7.1-16.8-10.4-27.1-10.4-19.7 0-36.7 15.8-38.1 37h4.9l-7.9 15.1-7.9-15.1h4.3c1.4-24.2 21.4-43.6 44.7-43.6 12 0 23.2 4.3 31.6 12.6 8.5 8.4 13.1 19.4 13.1 31.3s-4.6 23-13.1 31.4c-8.4 8.4-19.7 12.8-31.6 12.8zm-42.1-40.7l.9 1.8.9-1.8h-1.8z"/></svg>
					</i>
					<h3>3. You're Done</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.</p>
					<a class="btn-blk btn-outline">Login</a>
				</div>
			</div>
		</div>
	</section>
<?php get_footer(); ?>